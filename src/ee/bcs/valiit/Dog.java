package ee.bcs.valiit;

/**
 * This is a Dog class
 */

public class Dog extends Organism implements Mammal {

    public static final int LOVE = 30;
    private String naming;

    public String getNaming() {
        return naming;
    }

    public void setNaming(String naming) {
        this.naming = naming;
    }

    public void bark() {
        System.out.println("Auh-auh!");
    }

    public int leg_count(int count) {
        if (count > 2) {
            System.out.println("Tegemist on koeraga!");
        } else {
            System.out.println("Tegu on inimesega");
        }
        return 0;
    }

    public String name(String dog_name) {
        System.out.println(dog_name);
        return dog_name;
    }
}
