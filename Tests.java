package ee.bcs.valiit;

import java.math.BigInteger;

public class Tests {
    public static boolean test(int parameter) {
        if (parameter>10) {
            return true;
        } else {
            return false;
        }
    }
    public static void test2 (String one, String two) {
        int testingNumber;
    }

    public static double addVat(double input) {
        return input+1;
    }
    public static int[] GoingCrazy(int three, int four, boolean isMassive) {
        int[] Massive = new int[] {three, four};
        return Massive;
    }

    public static String deriveGender(String personalCode) {
        if (personalCode.charAt(0)%2 == 1)  {
            return "M";
        } else {
            return "F";
        }
    }
    public static void printHello() {
        System.out.println("Hello!");
    }

    /**
     *
     * @param personsCode
     * @return aasta
     */
    public static int deriveBirthYear(String personsCode) {
        int finalNumber = 0;
        String birthYear = Character.toString(personsCode.charAt(1)) + Character.toString(personsCode.charAt(2));
        if (Character.getNumericValue(personsCode.charAt(0)) <= 2) {
            String final_year = "18" + birthYear;
            finalNumber = Integer.parseInt(final_year);
        } else if (Character.getNumericValue(personsCode.charAt(0)) <= 4 && Character.getNumericValue(personsCode.charAt(0)) > 2) {
            String final_year = "19" + birthYear;
            finalNumber = Integer.parseInt(final_year);
        } else if (Character.getNumericValue(personsCode.charAt(0)) == 5 || Character.getNumericValue(personsCode.charAt(0)) == 6) {
            String final_year = "20" + birthYear;
            finalNumber = Integer.parseInt(final_year);
        }
        return finalNumber;
    }

    public static boolean validatePersonalCode(BigInteger Code) {
        String pattern = "[1-6][0-9]{2}[1,2][0-9][0-9]{2}[0-9]{4}";
        if (Code.toString().matches(pattern)) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        BigInteger number = new BigInteger("49410010299");
        String gender;

        System.out.println(test(5));

        test2("Tere", "tore");

        System.out.println(addVat(10.5));

        GoingCrazy(2,3, false);

        gender = deriveGender("39603760234");
        System.out.println(gender);

        printHello();

        System.out.println(deriveBirthYear("39603760234"));

        System.out.println(validatePersonalCode(number));
    }

}
